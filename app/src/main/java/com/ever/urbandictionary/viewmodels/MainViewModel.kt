package com.ever.urbandictionary.viewmodels

interface MainViewModel {
    fun onSearchViewSubmitted(term: String)
    fun sortList(sortBy: String)
}
