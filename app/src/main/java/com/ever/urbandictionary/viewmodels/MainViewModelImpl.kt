package com.ever.urbandictionary.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ever.urbandictionary.UrbanDicRepository
import com.ever.urbandictionary.models.Item
import kotlinx.coroutines.*

class MainViewModelImpl : MainViewModel, ViewModel() {

    private val urbDicRepo: UrbanDicRepository = UrbanDicRepository.getInstance()

    private val _definitionList = MutableLiveData<List<Item>>()
    val definitionList: LiveData<List<Item>> = _definitionList

    private val _showProgress: MutableLiveData<Boolean> = MutableLiveData()
    val showProgress: LiveData<Boolean> = _showProgress

    override fun onSearchViewSubmitted(term: String) {
        _showProgress.value = true
        viewModelScope.launch {
            _definitionList.value = urbDicRepo.getDefinitions(term)
            _showProgress.value = false
        }
    }

    override fun sortList(sortBy: String) {
        viewModelScope.launch {
            val itemList = _definitionList.value
            when (sortBy) {
                "up" -> _definitionList.value = itemList?.sortedBy { it.thumbsUp }
                "down" -> _definitionList.value = itemList?.sortedBy { it.thumbsDown }
            }
        }
    }
}
