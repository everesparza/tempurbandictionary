package com.ever.urbandictionary

import android.util.Log
import com.ever.urbandictionary.models.Item
import com.ever.urbandictionary.network.UrbanDictionaryRetrofit
import com.ever.urbandictionary.network.UrbanDictionaryService


class UrbanDicRepository private constructor() {
    private val TAG = UrbanDicRepository::class.java.simpleName

    suspend fun getDefinitions(term: String): List<Item> {
        var itemList = listOf<Item>()

        val response = UrbanDictionaryRetrofit
            .get()
            .create(UrbanDictionaryService::class.java)
            .getDefinitions(term)

        if (response.isSuccessful) {
            itemList = response.body()?.list ?: listOf()
        } else {
            Log.e(TAG, "Error while retrieving definitions for term: $term, ${response.message()}")
        }

        return itemList
    }

    companion object {
        @Volatile
        private var instance: UrbanDicRepository? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance ?: UrbanDicRepository().also { instance = it }
        }
    }
}
