package com.ever.urbandictionary.views

import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import com.ever.urbandictionary.UrbanDictionaryAdapter
import com.ever.urbandictionary.databinding.ActivityMainBinding
import com.ever.urbandictionary.viewmodels.MainViewModelImpl
import kotlinx.android.synthetic.main.activity_main.*
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.ever.urbandictionary.R

class MainActivity : BaseActivity(), View.OnClickListener {

    private val urbanDicViewModel: MainViewModelImpl by lazy {
        ViewModelProviders.of(this).get(MainViewModelImpl::class.java)
    }

    private var recyclerAdapter: UrbanDictionaryAdapter = UrbanDictionaryAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpBindings()
        setUpRecyclerView()
        setListeners()
        setSubscribers()
    }

    private fun setUpBindings() {
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        binding.viewModel = urbanDicViewModel
    }

    private fun setUpRecyclerView() {
        search_recycler.apply {
            itemAnimator = DefaultItemAnimator()
            adapter = recyclerAdapter
        }
    }

    private fun setListeners() {
        searchBox.setOnEditorActionListener { view, actionId, _ ->

            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    searchTerm(view)
                    return@setOnEditorActionListener true
                }
                else -> {
                    false
                }
            }
        }

        searchBox.setOnKeyListener { view, keyCode, keyEvent ->
            if (keyEvent.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                searchTerm(view as TextView)
                return@setOnKeyListener true
            }
            false
        }

        sortThumbsDown.setOnClickListener(this)
        sortThumbsUp.setOnClickListener(this)
    }

    private fun searchTerm(view: TextView) {
        view.hideKeyboard()
        val term = with(view) { text.toString() }
        progressBar.visibility = View.VISIBLE
        urbanDicViewModel.onSearchViewSubmitted(term)
    }

    private fun setSubscribers() {
        (urbanDicViewModel as MainViewModelImpl).definitionList.observe(this, Observer { list ->
            list?.let {
                recyclerAdapter.updateList(list)
            }
        })
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.sortThumbsUp ->
                urbanDicViewModel.sortList("up")
            R.id.sortThumbsDown ->
                urbanDicViewModel.sortList("down")
        }
    }
}
