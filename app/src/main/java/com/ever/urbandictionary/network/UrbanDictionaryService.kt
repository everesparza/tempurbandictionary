package com.ever.urbandictionary.network

import com.ever.urbandictionary.models.SearchItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface UrbanDictionaryService {

    @GET("define")
    suspend fun getDefinitions(@Query("term") term: String)
            : Response<SearchItem>

}
