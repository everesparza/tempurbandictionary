package com.ever.urbandictionary

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ever.urbandictionary.models.Item

class UrbanDictionaryAdapter(var searchItemList: List<Item> = listOf()) :
    RecyclerView.Adapter<UrbanDictionaryAdapter.CustomViewHolder>() {

    lateinit var mContext: Context

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.description.text = searchItemList[position].definition
        holder.thumbsUp.text = searchItemList[position].thumbsUp.toString()
        holder.thumbsDown.text = searchItemList[position].thumbsDown.toString()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        mContext = parent.context

        val view: View =
            LayoutInflater
                .from(mContext)
                .inflate(R.layout.search_item, parent, false)

        return CustomViewHolder(view)
    }

    class CustomViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var description: TextView = v.findViewById(R.id.description)
        var thumbsUp: TextView = v.findViewById(R.id.thumbsUp)
        var thumbsDown: TextView = v.findViewById(R.id.thumbsDown)
    }

    override fun getItemCount(): Int = searchItemList.size

    fun updateList(newList: List<Item>) {
        searchItemList = newList
        notifyDataSetChanged()
    }

}
