package com.ever.urbandictionary

import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.ever.urbandictionary.models.Item
import com.ever.urbandictionary.viewmodels.MainViewModelImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Assert
import org.mockito.Mockito.mock

import kotlin.coroutines.CoroutineContext


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@ExperimentalCoroutinesApi
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

}
